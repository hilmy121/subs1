package com.example.akhmadhilmy.kotlinlearn

import android.annotation.TargetApi
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity


class MainActivity : AppCompatActivity() {

    private var items: MutableList<Item> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        initData()

        club_list.layoutManager = LinearLayoutManager(this)
        club_list.adapter = RecyclerViewAdapter(this,items){
            startActivity<ClubDesc>(ClubDesc.Thisitem to it)

        }




    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun initData(){
        val name = resources.getStringArray(R.array.Club_name)
        val image = resources.obtainTypedArray(R.array.Club_Logo)
        val desc = resources.getStringArray(R.array.ClubDesc)
        items.clear()
        for (i in name.indices) {
            items.add(Item(name[i],
                    image.getResourceId(i,0),desc[i]))

        }


        //Recycle the typed array
        image.recycle()
    }

}

