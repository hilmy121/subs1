package com.example.akhmadhilmy.kotlinlearn

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Akhmad Hilmy on 9/16/2018.
 */
@Parcelize
data class Item (val name: String?, val image: Int?, val details : String?) : Parcelable