package com.example.akhmadhilmy.kotlinlearn

import android.content.ClipData
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout.view.*

/**
 * Created by Akhmad Hilmy on 9/16/2018.
 */


class RecyclerViewAdapter(private val context: Context, private val items: MutableList<Item>, private val listener :(Item) -> Unit)
    :RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position],listener)
    }


    override fun getItemCount(): Int = items.size

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),LayoutContainer{




        fun bindItem(items: Item, listener: (Item) -> Unit) {
            itemView.name.text=items.name
            Glide.with(itemView.context).load(items.image).into(itemView.imageBarca)
            itemView.setOnClickListener{
                listener(items)
            }
        }
    }
}