package com.example.akhmadhilmy.kotlinlearn

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import com.bumptech.glide.Glide
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7._ListMenuItemView

/**
 * Created by Akhmad Hilmy on 9/22/2018.
 */
class ClubDesc : AppCompatActivity() {

    companion object {
        val Thisitem = "This Item"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val ThisIntent = intent
        val hili = intent.getParcelableExtra<Item>(Thisitem)
        detailLayout(hili).setContentView(this)
    }

}
class detailLayout(var list: Item) : AnkoComponent<ClubDesc>{
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun createView(ui: AnkoContext<ClubDesc>)= with(ui) {

        verticalLayout{
            lparams(matchParent, matchParent)
            imageView{
                Glide.with(this).load(list.image).into(this)
                this@verticalLayout.gravity=Gravity.CENTER_HORIZONTAL
            }
            textView{
                text = list.name
                textSize = sp(15).toFloat()
            }
            textView{
                text = list.details
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                gravity = Gravity.CENTER_HORIZONTAL

            }

        }
    }
}